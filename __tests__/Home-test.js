import React from 'react';
import renderer from 'react-test-renderer';
import Categoria from 'src/components/Categoria';
import Favorito from 'src/components/Favorito';
import Restaurante from 'src/components/Restaurante';

describe('Test de componentes de screen home', () => {
    const TEST_DATA = {
        image: require('src/assets/img/categorias/hamburguesas.png'),
        name : 'Test Categoria',
    };
    test('Categoria Component', () => {
        const tree = renderer.create(<Categoria categoria={TEST_DATA}/>).toJSON();
        expect(tree).toMatchSnapshot();
      });
    test('Favorito component', () => {
        const tree = renderer.create(<Favorito favorito={TEST_DATA}/>).toJSON();
        expect(tree).toMatchSnapshot();
    });
    test('Restaurante component', () => {
        const tree = renderer.create(<Restaurante />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});


