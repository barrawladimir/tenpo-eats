import React from 'react';
import renderer from 'react-test-renderer';
import Error from 'src/components/Error';
import Title from 'src/components/RoundedInput';
import RoundedInput from 'src/components/Title';

describe('Test de componentes ', () => {
    test('Error Component', () => {
        const tree = renderer.create(<Error seccion="test" error={true}/>).toJSON();
        expect(tree).toMatchSnapshot();
      });
    test('Title component', () => {
        const tree = renderer.create(<Title>Test</Title>).toJSON();
        expect(tree).toMatchSnapshot();
    });
    test('RoundedInput component', () => {
        const tree = renderer.create(<RoundedInput />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
