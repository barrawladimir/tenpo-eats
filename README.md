# Tenpo Eats

Desarrollo de desafío para Tenpo Front. En este desafío desarrolle la vista HOME, la funcionalidad extra es la screen agregar dirección, la cual se le agrego una simulación de mapa al ingresar una dirección.

## Instalación de paquetes necesarios

### Windows

```bash
npm install
```

### Mac 
Sí no tienes cocoapods debes instalarlo con este comando
```bash
sudo gem install cocoapods
```
Luego en la carpeta del proyecto
```bash
npm install
cd ios
pod install
cd ..
```

## Probar la aplicación

### Windows
```bash
npx react-native run-android
```

### Mac
```bash
npx react-native run-ios
```

## Revisión de tests
Esta aplicación cuenta con test de componentes realizado con jest
para revisarlos debes correr el siguiente comando‹
```bash
npm run test
```

