import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    show_error:{
        marginTop: 20,
        fontFamily: 'Roboto-Light',
    },

    hide_error:{
        display: 'none',
    },

    input_text:{
        fontSize: 17,
        color: '#32a89a',
    },

    rounded_input:{
        backgroundColor:'#FFFFFF',
        borderWidth: 1,
        borderRadius: 50,
        paddingVertical:20,
        paddingHorizontal:25,
        borderColor: '#D6DBDF',
        width: '100%',
        shadowColor: '#00000029',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },

});
