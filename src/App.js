import React from 'react';
import HomeScreen from 'src/scenes/Home/Home.screen';
import UbicacionScreen from 'src/scenes/Ubicacion/Ubicacion.screen';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const App = () => {

    const RootStack = createStackNavigator();

    return (
        <NavigationContainer>
            <RootStack.Navigator mode="modal" headerMode="none">
                <RootStack.Screen name="Home" component={HomeScreen} />
                <RootStack.Screen name="Ubicacion" component={UbicacionScreen} />
            </RootStack.Navigator>
        </NavigationContainer>
     );
};

export default App;
