import React from 'react';
import { View, Image, Text} from 'react-native';
import styles from './Categoria.style';

const Categoria = ({categoria = {}}) => {

    const { image, name } = categoria;

    return (
        <View style={styles.container}>
            <Image style={styles.image} source={image}/>
            <Text style={styles.text_categoria}>{name}</Text>
        </View>
     );
};

export default Categoria;
