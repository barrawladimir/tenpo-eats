import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container:{
        alignItems: 'center',
        marginTop: 10,
        justifyContent: 'center',
    },

    image: {
        width: 180,
        height: 80,
    },

    text_categoria:{
        position: 'absolute',
        margin: 'auto',
        color: '#FFFFFF',
        fontSize: 20,
        fontFamily: 'Roboto-Medium',
    },
});
