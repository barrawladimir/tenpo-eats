import React from 'react';
import { View, Image,Text } from 'react-native';
import styles from './Restaurante.style';

const Restaurante = ({restaurante = {}}) => {

    const {discount, image, name, rate, wait_time} = restaurante;

    return (
        <View style={styles.container}>
            <Descuento discount={discount}/>
            <Image style={styles.img_restaurante} source={image} />
            <Text style={styles.title_restaurante}>{name}</Text>
            <Text style={styles.time_rate}>
                <Image style={styles.star} source={require('src/assets/icons/estrella.png')} />
                {`${rate} ${wait_time}`}
            </Text>
        </View>
     );
};


const Descuento = ({discount}) => {

    return (
        <View style={styles.descuento}>
            <Text style={styles.text_descuento}>{discount}%{'\n'} DCTO.</Text>
        </View>
    );
};

export default Restaurante;
