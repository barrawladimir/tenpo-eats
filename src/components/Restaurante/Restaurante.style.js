import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container:{
        height: 150,
        flexDirection:'column',
        width: 100,
        marginRight:10,
        paddingTop:20,
        marginBottom:20,
    },

    descuento:{
        position: 'absolute',
        zIndex: 100,
        paddingVertical: 7,
        backgroundColor: '#00baa4',
        borderRadius: 1000,
        right:-10,
        top: 10,
    },

    text_descuento:{
        textAlign: 'center',
        fontSize: 8,
        color: '#FFFFFF',
        fontFamily: 'Gotham-Light',
    },

    img_restaurante:{
        width: 100,
        height: 100,
    },

    title_restaurante:{
        marginTop:5,
        textAlign:'center',
        fontFamily: 'Roboto-Light',
    },

    star:{
        width: 10,
        height: 10,
    },

    time_rate:{
        textAlign: 'center',
        fontFamily: 'Roboto-Light',
        fontSize: 12,
        marginTop:3,
    },
});
