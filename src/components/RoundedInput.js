import React from 'react';
import { TextInput, View } from 'react-native';
import defaultStyles from 'src/styles/defaultStyles';

const RoundedInput = ({...props}) => {
    return (
        <View style={defaultStyles.rounded_input}>
                <TextInput
                    style={defaultStyles.input_text}
                    {...props}
                />
        </View>
    );
};

export default RoundedInput;
