import React from 'react';
import { TouchableOpacity, Image, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {styles} from './Ubicacion.styles';
import iconUbicacion from 'src/assets/icons/ubicacion.png';

const UbicacionButton = ({datos}) => {

    const { ubicacion } = datos;

    const navigation = useNavigation();

    const { buttonAddDirection, iconButtonDirection, textButtonDirection, textUbicacion} = styles(ubicacion);

    return (
        <TouchableOpacity onPress={() => navigation.navigate('Ubicacion', {datosUbicacion :datos})} style={buttonAddDirection}>
            <Image style={iconButtonDirection} source={iconUbicacion}/>
            { datos && datos.ubicacion
                ?
                    <Text style={textButtonDirection}>Enviaremos tus pedidos a{'\n'}
                        <Text style={textUbicacion}>{ubicacion}</Text>
                    </Text>
                :
                <Text style={textButtonDirection}>Agregar dirección de entrega</Text>
            }
        </TouchableOpacity>
    );
};

export default UbicacionButton;
