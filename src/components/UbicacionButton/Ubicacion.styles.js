import { StyleSheet } from 'react-native';

export const styles = (ubicacion) => StyleSheet.create({
    buttonAddDirection:{
        flexDirection: 'row',
        width: '100%',
        backgroundColor: '#D4F9F5',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        height: ubicacion ? 140 : 130,
        justifyContent: 'center',
    },

    textButtonDirection:{
        marginBottom: 'auto',
        marginTop: 23,
        color: '#028f7e',
        fontSize: ubicacion ? 14 : 16,
        marginLeft: 10,
        fontFamily: 'Gotham-Light',

    },

    iconButtonDirection:{
        width: ubicacion ? 30 : 20,
        marginTop: ubicacion ? 23 : 23,
        height: ubicacion ? 40 : 20,
    },

    textUbicacion:{
        fontSize: 18,
        fontFamily: 'Gotham-Light',
    },
});
