import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    title:{
        fontFamily: 'Gotham-Bold',
        fontSize: 18,
        textTransform: 'uppercase',
    },
});
