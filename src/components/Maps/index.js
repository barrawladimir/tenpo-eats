import React, { useEffect, useRef } from 'react';
import {  Image, Animated,Text} from 'react-native';
import imageMaps from 'src/assets/img/ubicacion/maps.png';
import styles from './Maps.styles';

//Solo es una simulación de maps.
const Maps = ({ubicacion}) =>{

    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        if ( ubicacion && ubicacion.length > 0){
        Animated.timing(
          fadeAnim,
          {
            toValue: 1,
            duration: 2000,
            useNativeDriver: true,
          }
        ).start();
        }
    }, [fadeAnim, ubicacion]);

    let animatedStyle = {opacity: fadeAnim, zIndex:1};

    return (
        <>
            { ubicacion === undefined &&
                <Text style={styles.sin_ubicacion_text}>Ingresa una ubicación para cargar el mapa.</Text>
            }
            <Animated.View style={animatedStyle} >
                <Image source={imageMaps} style={styles.map_component}/>
            </Animated.View>
        </>
    );
};

export default Maps;
