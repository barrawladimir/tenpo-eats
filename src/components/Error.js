import React from 'react';
import { Text } from 'react-native';
import defaultStyles from 'src/styles/defaultStyles';

const Error = ({seccion, error}) => {

    const style =  error ? defaultStyles.show_error : defaultStyles.hide_error;

    return (
        <Text style={style}>
            ¡Ouch! Ocurrío un error al obtener {seccion}, por favor reintenta.
        </Text>
    );
};

export default Error;
