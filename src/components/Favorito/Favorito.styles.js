import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container:{
        marginTop:20,
        width:270,
        marginBottom: 30,
    },

    card_footer:{
        padding: 10,
        marginHorizontal:5,
        backgroundColor: '#ffffff',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginBottom: 40,
        borderBottomEndRadius: 10,
        borderBottomStartRadius: 10,
    },

    img_restaurante:{
        width:40,
        height:40,
        left:15,
        top: 10,
    },

    img_background:{
        width:270,
        height:100,
    },

    text_comida:{
        fontFamily: 'Roboto-Light',
        fontSize: 12,
    },

    text_restaurante:{
        fontFamily: 'Roboto-Bold',
        color: '#00baa4',
        fontSize: 12,
        marginTop: 5,
        flexDirection: 'column',
    },

    container_flex:{
        flex: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },

    star:{
        width: 10,
        height: 10,
        marginRight: 10,
    },

});


