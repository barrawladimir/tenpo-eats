import React from 'react';
import {View, Text, Image, ImageBackground} from 'react-native';
import styles from './Favorito.styles';

const Favorito = ({favorito = {}}) => {

    const { back_image, restaurant_image, title, rate, wait_time, restaurant } = favorito;

    return (
        <View style={styles.container}>
            <ImageBackground source={back_image} style={styles.img_background}>
                <Image style={styles.img_restaurante} source={restaurant_image}/>
            </ImageBackground>
            <View style={styles.card_footer}>
                <View style={styles.container_flex}>
                    <Text style={styles.text_comida}>{title}</Text>
                    <Text style={styles.text_comida}>
                        <Image style={styles.star} source={require('src/assets/icons/estrella.png')} />
                        {rate}
                    </Text>
                </View>
                <View style={styles.container_flex}>
                    <Text style={styles.text_restaurante}>{restaurant}</Text>
                    <Text style={styles.text_restaurante}>
                        {wait_time}
                    </Text>
                </View>
            </View>
        </View>
     );
};

export default Favorito;
