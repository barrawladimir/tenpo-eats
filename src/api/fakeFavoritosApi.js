
export const getFavoritos = () => {
    try {
        return Promise.resolve({
            status: 200,
            data: [
                {
                    back_image: require('src/assets/img/favoritos/hamburguesa.png'),
                    restaurant_image: require('src/assets/img/restaurantes/mc_donalds.png'),
                    title: 'Combo Hamburguesa Big Mac',
                    rate: 3.5,
                    wait_time: '10-50 min.',
                    restaurant: 'Mc Donalds',
                    id: 1,
                },
                {
                    back_image: require('src/assets/img/favoritos/pizza.png'),
                    restaurant_image: require('src/assets/img/restaurantes/papa_johns.png'),
                    title: 'Pizza Mediana 3 Ingredientes',
                    rate: 4.5,
                    wait_time: '30-60 min.',
                    restaurant: 'Papa Johns',
                    id:2,
                },
                {
                    back_image: require('src/assets/img/favoritos/hamburguesa.png'),
                    restaurant_image: require('src/assets/img/restaurantes/mc_donalds.png'),
                    title: 'Combo Hamburguesa Guacamole',
                    rate: 3.5,
                    wait_time: '40-60 min.',
                    restaurant: 'Mc Donalds',
                    id: 3,
                },
                {
                    back_image: require('src/assets/img/favoritos/pizza.png'),
                    restaurant_image: require('src/assets/img/restaurantes/melt.png'),
                    title: 'Pizza Familiar 4 Ingredientes',
                    wait_time: '20-60 min.',
                    restaurant: 'MELT Pizzas',
                    rate: 4.0,
                    id:4,
                },
            ],
        });
    }
    catch (error) {
        return Promise.reject(error);
    }
};
