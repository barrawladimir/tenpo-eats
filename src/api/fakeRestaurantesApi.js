
export const getRestaurantes = () => {
    try {
        return Promise.resolve({
            status: 200,
            data: [
                {
                    image: require('src/assets/img/restaurantes/mc_donalds.png'),
                    name: 'Mc Donalds',
                    rate: '3.5',
                    wait_time: '10-50 min.',
                    discount: 50,
                    id: 1,
                },
                {
                    image: require('src/assets/img/restaurantes/melt.png'),
                    name: 'MELT Pizzas',
                    rate: '4.5',
                    wait_time: '10-60 min.',
                    discount: 30,
                    id: 2,
                },
                {
                    image: require('src/assets/img/restaurantes/yokono.png'),
                    name: 'YOKONO',
                    rate: '3.5',
                    wait_time: '10-50 min.',
                    discount: 20,
                    id:3,
                },
                {
                    image: require('src/assets/img/restaurantes/papa_johns.png'),
                    name: 'Papa Johns',
                    rate: '4.5',
                    wait_time: '10-60 min.',
                    discount: 15,
                    id: 4,
                },
            ],
        });
    }
    catch (error) {
        return Promise.reject(error);
    }
};
