
export const getCategorias = () => {
    try {
        return Promise.resolve({
            status: 200,
            data: [
                {
                    image: require('src/assets/img/categorias/hamburguesas.png'),
                    name: 'Hamburguesas',
                    id: 1,
                },
                {
                    image: require('src/assets/img/categorias/italiana.png'),
                    name: 'Italiana',
                    id: 2,
                },
                {
                    image: require('src/assets/img/categorias/pizzas.png'),
                    name: 'Pizzas',
                    id:3,
                },
            ],
        });
    }
    catch (error) {
        return Promise.reject(error);
    }
};
