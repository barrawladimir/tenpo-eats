import React, { useEffect, useState } from 'react';
import { SafeAreaView, Image, View, Text , TextInput, TouchableOpacity, Alert,ScrollView} from 'react-native';
import RoundedInput from 'src/components/RoundedInput';
import Maps from 'src/components/Maps';
import iconUbicacion from 'src/assets/icons/ubicacion.png';
import styles from './Ubicacion.styles';

const Ubicacion = ({ route, navigation }) => {
    const { datosUbicacion: getDatos } = route.params ?? {};

    const [datosUbicacion,setUbicacion] = useState({
        ubicacion: undefined,
        info_extra: undefined,
    });

    const { ubicacion, info_extra } = datosUbicacion;

    const onChangeDatos = (name) => (value) => {
        setUbicacion({...datosUbicacion, [name]: value});
    };

    const handleGoHome = () => {
        if (!ubicacion || ubicacion.length < 1){
            Alert.alert(
                'Whoops',
                'Estás seguro que deseas volver sin guardar una dirección?',
                [
                    {
                        text: 'Cancelar',
                        style: 'cancel',
                    },
                    {
                        text:'Sí',
                        onPress: () => navigation.navigate('Home', {datosUbicacion}),
                    },
                ],
            );
        }
        else {
            navigation.navigate('Home', {datosUbicacion});
        }

    };

    useEffect(() => {
        if (getDatos){
            setUbicacion({...getDatos});
        }
    },[getDatos]);

    return (
        <ScrollView>
        <SafeAreaView style={styles.head_container}>
            <View style={styles.container_direccion}>
                <Text style={styles.direccion_text}>
                    <Image source={iconUbicacion} style={styles.img_ubicacion}/>
                    Agregar dirección de entrega
                </Text>
            </View>
            <View style={styles.container_input}>
                <RoundedInput
                    onChangeText={onChangeDatos('ubicacion')}
                    value={ubicacion}
                />
            </View>
        </SafeAreaView>
        <Maps ubicacion={ubicacion}/>
        <View style={styles.extra_container}>
            <Text style={styles.extra_info_text}>Agregar información de entrega</Text>
            <Text style={styles.extra_subtitle}>Depto, Oficina, Piso, Block</Text>
            <TextInput
                style={styles.text_area}
                multiline={true}
                numberOfLines={4}
                onChangeText={onChangeDatos('info_extra')}
                value={info_extra}
            />
            <TouchableOpacity onPress={handleGoHome} style={styles.save_button}>
                <Text style={styles.save_text}>Agregar Dirección</Text>
            </TouchableOpacity>
        </View>
        </ScrollView>
     );
};

export default Ubicacion;
