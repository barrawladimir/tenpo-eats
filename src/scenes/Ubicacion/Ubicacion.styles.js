import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    direccion_text:{
        color: '#32a89a',
        alignSelf: 'center',
        fontFamily: 'Gotham-Light',
        fontSize: 18,
    },
    extra_info_text:{
        fontFamily: 'Gotham-Bold',
        fontSize: 16,
    },
    extra_container:{
        paddingHorizontal:10,
        backgroundColor: '#FFFFFF',
        paddingVertical: 40,
        flex:1,
    },
    text_area:{
        borderWidth:1,
        height:170,
        borderRadius: 16,
        borderColor: '#E8E8E8',
        padding: 10,
    },
    extra_subtitle:{
        fontFamily: 'Gotham-Light',
        fontSize:14,
        color: '#99A3A4',
        marginTop: 5,
        marginBottom: 15,
    },
    save_button:{
        width: '80%',
        alignSelf:'center',
        backgroundColor:'#01bba4',
        padding: 20,
        borderRadius:10,
        marginTop:35,
        alignItems:'center',
    },
    save_text:{
        color:'#FFFFFF',
        textTransform: 'uppercase',
        fontFamily: 'Gotham-Bold',
    },
    head_container:{
        backgroundColor:'#D4F9F5',
        height:200,
        zIndex:40,
    },
    container_direccion:{
        bottom: -80,
        alignItems:'center',
        alignContent:'center',
    },
    img_ubicacion:{
        width: 20,
        height: 20,
    },
    container_input:{
        position:'absolute',
        bottom:-30,
        width:'100%',
    },
});
