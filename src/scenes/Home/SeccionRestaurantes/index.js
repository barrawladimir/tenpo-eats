import React, { useEffect, useRef, useState } from 'react';
import { Animated } from 'react-native';
import { getRestaurantes } from '../../../api/fakeRestaurantesApi';
import styles from '../Home.style';
import Title from 'src/components/Title';
import Restaurante from 'src/components/Restaurante';
import Error from 'src/components/Error';

const SeccionRestaurantes = () => {
    const [restaurantes,setRestaurantes] = useState([]);
    const [error,setError] = useState(false);

    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        getRestaurantes().then(resp => {
            if (resp.status === 200){
                setRestaurantes(resp.data);
            }
            else {
                setError(true);
            }
        }, () => setError(true));
    }, []);

    useEffect(() => {
        Animated.timing(
          fadeAnim,
          {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
          }
        ).start();
      }, [fadeAnim]);

    let styleContainer = restaurantes.length > 0 ? { ...styles.restaurantes_container, opacity: fadeAnim} :  { ...styles.restaurantes_container};

    return (
        <>
        <Title>Restaurantes</Title>
        <Error seccion="Restaurantes" error={error}/>
        <Animated.ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{...styleContainer}}
        >
            {Array.isArray(restaurantes) && restaurantes.length > 0 && error === false &&
                restaurantes.map(restaurante =>
                    <Restaurante key={restaurante.id} restaurante={restaurante}/>
                )
            }
        </Animated.ScrollView>
    </>
     );
};



export default SeccionRestaurantes;
