import React, { useEffect, useState } from 'react';
import {ScrollView, View} from 'react-native';
import styles from './Home.style';
import Header from './Header';
import UbicacionButton from 'src/components/UbicacionButton';
import SeccionRestaurantes from './SeccionRestaurantes';
import SeccionCategorias from './SeccionCategorias';
import SeccionFavoritos from './SeccionFavoritos';

const HomeScreen = ({ route }) => {
    const {datosUbicacion: getUbicacion} = route.params ?? {};

    const [datosUbicacion,setUbicacion] = useState({});

    useEffect(() => {
        setUbicacion({...getUbicacion});
    },[getUbicacion]);

    return (
        <ScrollView
            style={styles.container}
            vertical={true}
            showsVerticalScrollIndicator={false}
        >
            <Header/>
            <UbicacionButton datos={datosUbicacion}/>
            <View style={styles.container_menu}>
                <SeccionRestaurantes/>
                <SeccionCategorias />
                <SeccionFavoritos />
            </View>
        </ScrollView>
     );
};

export default HomeScreen;
