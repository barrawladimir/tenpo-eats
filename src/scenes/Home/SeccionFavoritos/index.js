import React, { useEffect, useRef, useState } from 'react';
import { View, Animated } from 'react-native';
import Error from 'src/components/Error';
import { getFavoritos } from 'src/api/fakeFavoritosApi';
import Title from 'src/components/Title';
import Favorito from '../../../components/Favorito';
import styles from '../Home.style';

const SeccionFavoritos = () => {

    const [favoritos,setFavoritos] = useState([]);
    const [error, setError] = useState(false);

    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        getFavoritos().then(resp => {
            if (resp.status === 200){
                setFavoritos(resp.data);
            }
            else {
                setError(true);
            }
        }, () => setError(true));
    },[]);

    useEffect(() => {
        Animated.timing(
          fadeAnim,
          {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
          }
        ).start();
    }, [fadeAnim]);

    let styleContainer = favoritos.length > 0 && {opacity: fadeAnim};

    return (
        <>
        <View style={styles.container_margin}>
            <Title>Tus favoritos</Title>
            <Error seccion="Favoritos" error={error} />
            <Animated.ScrollView
                style={{...styleContainer}}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            >
                {Array.isArray(favoritos) && favoritos.length > 0 && error === false &&
                    favoritos.map(favorito =>
                        <Favorito key={favorito.id} favorito={favorito} />
                    )
                }
            </Animated.ScrollView>
        </View>
        </>
     );
};

export default SeccionFavoritos;
