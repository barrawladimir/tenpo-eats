/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import {Dimensions, Animated, Easing, View, Text, Image, TouchableOpacity} from 'react-native';
import styles from '../Home.style';

const Header = () => {
    let left = new Animated.Value(0);
    let top = new Animated.Value(0);

    const windowWidth = Dimensions.get('window').width;

    const animateHorizontaly = easing => {
        left.setValue(windowWidth / 2);
        Animated.timing(left, {
          toValue: windowWidth / 2.2,
          duration: 1200,
          useNativeDriver: false,
          easing,
        }).start();
    };

    const animateVerticaly = easing => {
        top.setValue(-10);
        Animated.timing(top, {
          toValue: 10,
          duration: 1000,
          useNativeDriver: false,
          easing,
        }).start();
    };

    useEffect(() => {
        animateHorizontaly(Easing.ease);
        animateVerticaly(Easing.ease);

    },[]);

    const animatedStyles =
        {
          left,
          top,
          width: 100,
          height: 100,
          position:'absolute',
          zIndex: 0,
          alignSelf: 'center',
        };
    return (
        <>
            <View style={styles.container_actions}>
                <TouchableOpacity>
                    <Image style={styles.icon_user} source={require('src/assets/img/user.png')}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Image style={styles.icon_search} source={require('src/assets/img/search.png')}/>
                </TouchableOpacity>
            </View>
            <View style={styles.header}>
                <Text style={styles.home_title}>
                    Tenpo {'\n'}
                    <Text style={styles.colored_title}>Eats</Text> {'\n'}
                    <Text style={styles.home_subtitle}>DELIVERY APP</Text>
                </Text>
                <Animated.View style={animatedStyles} />
                <Animated.Image  style={animatedStyles}  source={require('src/assets/img/motociclista.png')} />
                <Image style={styles.image_celular} source={require('src/assets/img/phone.png')} />
            </View>
        </>
     );
};

export default Header;
