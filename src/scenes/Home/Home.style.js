import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    home_title:{
        fontFamily: 'Roboto-Black',
        fontSize: 32,
        color: '#333333',
        elevation:20,
    },

    header:{
        marginVertical: 20,
        width:'100%',
        flexDirection: 'row',
        paddingHorizontal: 20,
    },

    colored_title:{
        color: '#00baa4',
    },

    home_subtitle:{
        fontFamily: 'Roboto-Light',
        letterSpacing: 2,
        fontSize: 14,
    },

    container:{
        width: '100%',
        flex: 1,
        backgroundColor: '#f2f2f2',
    },

    icon_user:{
        width: 50,
        height: 50,
    },

    icon_search:{
        width: 30,
        height: 30
    },

    container_menu:{
        paddingHorizontal: 15,
        paddingTop:30,
        backgroundColor: '#FFFFFF',
        top: -60,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        height: '75%',
    },

    restaurantes_container:{
        marginTop: 5,
    },

    container_margin:{
        marginTop: 20,
    },

    container_actions:{
        marginTop: 30,
        paddingHorizontal: 20,
        flex: 1,
        flexDirection:'row',
        justifyContent: 'space-between',
    },

    image_celular:{
        width:100,
        height: 150,
        position:'absolute',
        left: '70%',
    },
});
