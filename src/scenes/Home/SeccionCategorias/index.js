import React, { useEffect, useRef, useState } from 'react';
import { Animated } from 'react-native';
import { getCategorias } from 'src/api/fakeCategoriasApi';
import Title from 'src/components/Title';
import Error from 'src/components/Error';
import Categoria from '../../../components/Categoria';


const SeccionCategorias = () => {
    const [categorias,setCategorias] = useState([]);
    const [error, setError] = useState(false);

    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        getCategorias().then(resp => {
            if (resp.status === 200){
                setCategorias(resp.data);
            }
            else {
                setError(true);
            }
        }, () => setError(true));
    },[]);

    useEffect(() => {
        Animated.timing(
          fadeAnim,
          {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
          }
        ).start();
    }, [fadeAnim]);

    let styleContainer = categorias.length > 0 && {opacity: fadeAnim};

    return (
        <>
            <Title>Categorias</Title>
            <Error seccion="Categorias" error={error}/>
            <Animated.ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                style={{...styleContainer}}
            >
                {Array.isArray(categorias) && categorias.length > 0 && error === false &&
                    categorias.map(categoria =>
                        <Categoria key={categoria.id} categoria={categoria}/>
                    )
                }
            </Animated.ScrollView>
        </>
     );
};

export default SeccionCategorias;
