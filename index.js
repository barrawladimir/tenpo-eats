/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from 'tenpoeats/src/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
